from fastapi.testclient import TestClient
from .main import app

client = TestClient(app)


def test_check_health():
    res = client.get("/health")
    assert res.status_code == 200
